﻿using System;
using MongoDB.Driver;
using static System.Console;
using MongoDB.Bson;
using System.Linq;

namespace LikeAChat
{
    partial class Program
    {
        public static Users GetUser()
        {
            Write("Enter your nickname : ");
            string NickName = ReadLine();
            Write("Enter your password : ");
            string Password = null;
            while (true)
            {
                var key = ReadKey(true);
                if (key.Key == ConsoleKey.Enter)
                    break;
                Password += key.KeyChar;
            }

            Users user = new Users()
            {
                NickName = NickName,
                Password = Password
            };

            Source = NickName;

            return user;
        }

        public static bool LoginDB(int Choise)
        {
            // Choise 1 - check login, 2 - register login, 3 - exit
            bool status = true;

            if (Choise == 3)
            {
                WriteLine("Terminating...");
                Environment.Exit(-1);
            }

            _client = new MongoClient("mongodb://" + Server + ":27001");
            _database = _client.GetDatabase("LikeAChat");
            _collection = _database.GetCollection<Users>("Users");
            Users _user = GetUser();

            switch (Choise)
            {
                case 1:
                    WriteLine("********");
                    try
                    {
                        var result = _collection.Find<Users>(Builders<Users>.Filter.Eq("NickName", _user.NickName)).FirstOrDefaultAsync();

                        if (result.Result.Password == _user.Password)
                        {
                            Console.ForegroundColor = ConsoleColor.Magenta;
                            WriteLine($"\nYou are logged as: {_user.NickName}. Have a good time...\n");
                            Console.ResetColor();
                            Target = GetContacts();
                            if (Target == "")
                            {
                                WriteLine("Wrong contact. Press any key.");
                                status = false;
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Magenta;
                                WriteLine($"\nConversation with {Target}. Use Enter to send message. Press ESC for log out.\n\n");
                                Console.ForegroundColor = ConsoleColor.Green;
                                Write("2" + Target + ": ");
                                Console.ResetColor();
                            }

                        }
                        else
                        {
                            WriteLine("Password wrong. Press any key.");
                            status = false;
                        }
                    }
                    catch (NullReferenceException)
                    {
                        WriteLine("User doesn't exists or wrong password provided. Press any key.");
                        status = false;
                    }
                    break;
                case 2:
                    WriteLine("Register login...");
                    try
                    {
                        _collection.InsertOne(_user);
                        WriteLine("User created.");
                        goto case 1;
                    }
                    catch (Exception)
                    {
                        WriteLine("Failed to create login. Press any key.");
                        status = false;
                    }
                    break;
                default:
                    status = false;
                    break;
            }

            return status;
        }

        public static string GetContacts()
        {
            var all = _collection.Find(new BsonDocument());
            Console.WriteLine("Contact list: ");

            foreach (Users i in all.ToEnumerable())
            {
                Console.WriteLine("  " + i.NickName);
            }
            Write("\nType nickname: ");
            var result = ReadLine();

            return (all.ToEnumerable().Any(tr => tr.NickName.Equals(result, StringComparison.CurrentCultureIgnoreCase))) ? result : "";
        }
    }
}
