﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Threading;
using System.Threading.Tasks;
using static LikeAChat.Program;

public class ReceiveService : BackgroundService
{
    private readonly ILogger _logger;
    private static IConnection _connection;
    private static IModel _channel;

    public string Queue { get; private set; }
    public string Sender { get; private set; }

    public ReceiveService(ILoggerFactory loggerFactory)
    {
        _logger = loggerFactory.CreateLogger<ReceiveService>();
        InitRabbitMQ();
    }

    private void InitRabbitMQ()
    {
        Queue = QueueName;
        Sender = Source;

        var factory = new ConnectionFactory { HostName = "localhost" };

        _connection = factory.CreateConnection();
        _channel = _connection.CreateModel();
        _channel.QueueDeclare(Queue, false, false, false, null);
        _channel.BasicQos(0, 1, false);

        _connection.ConnectionShutdown += RabbitMQ_ConnectionShutdown;
    }

    protected override Task ExecuteAsync(CancellationToken stoppingToken)
    {
        stoppingToken.ThrowIfCancellationRequested();

        var consumer = new EventingBasicConsumer(_channel);
        consumer.Received += (ch, ea) =>
        {
            // received message  
            var content = System.Text.Encoding.UTF8.GetString(ea.Body);

            // handle the received message  
            HandleMessage(content);
            _channel.BasicAck(ea.DeliveryTag, false);
        };

        consumer.Shutdown += OnConsumerShutdown;
        consumer.Registered += OnConsumerRegistered;
        consumer.Unregistered += OnConsumerUnregistered;
        consumer.ConsumerCancelled += OnConsumerConsumerCancelled;

        _channel.BasicConsume(Queue, false, consumer);

        return Task.CompletedTask;
        }

    private void HandleMessage(string content)
    {
        _logger.LogInformation($"consumer received {content}");
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine($"\n{Source}: {content}");
        Console.ResetColor();
    }

    private void OnConsumerConsumerCancelled(object sender, ConsumerEventArgs e) { }
    private void OnConsumerUnregistered(object sender, ConsumerEventArgs e) { }
    private void OnConsumerRegistered(object sender, ConsumerEventArgs e) { }
    private void OnConsumerShutdown(object sender, ShutdownEventArgs e) { }
    private void RabbitMQ_ConnectionShutdown(object sender, ShutdownEventArgs e) { }

    public override void Dispose()
    {
        _channel.Close();
        _connection.Close();
        base.Dispose();
    }

    public static void CloseConnection()
    {
        _channel.Close();
        _connection.Close();
    }
}
