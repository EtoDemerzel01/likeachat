﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace LikeAChat
{
    class Users
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("NickName")]
        public string NickName { get; set; }

        [BsonElement("Password")]
        public string Password { get; set; }
    }

    
}
