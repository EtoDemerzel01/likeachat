﻿using MongoDB.Driver;
using System;
using static System.Console;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace LikeAChat
{
    partial class Program
    {
        protected static IMongoClient _client;
        protected static IMongoDatabase _database;
        protected static IMongoCollection<Users> _collection;

        public static string Server { get; set; }
        public static string Password { get; set; }

        public static string Source { get; set; }
        public static string Target { get; set; }
        public static string QueueName { get; set; }

        static void Main(string[] args)
        {
            if (args.Length == 0 || args.Length > 2)
            {
                WriteLine("Proper usage: LikeAChat server password");
                return;
            }

            Server = args[0];
            Password = args[1];

            while (LoginDB(Login()) == false)
            {
                ReadKey(true);
                Console.Clear();
            }

            QueueName = Tools.GetQueueName(Source, Target);
            Task.Run(Reciver);

            string message = "";
            while (true)
            {
                var key = ReadKey();
                if (key.Key == ConsoleKey.Escape)
                {
                    WriteLine();
                    break;
                }

                if (key.Key == ConsoleKey.Backspace && message.Length > 0)
                {
                    Console.Write(" \b");
                    message = message.Substring(0, message.Length - 1);
                }
                    
                if (key.Key == ConsoleKey.Enter)
                {
                    // send message
                    Producer.Send(message);
                    Console.ForegroundColor = ConsoleColor.Green;
                    Write($"\n2{Target}: ");
                    Console.ResetColor();
                    message = "";
                }
                    
                message =  key.KeyChar >= 48 && key.KeyChar <= 122 ? String.Concat(message, key.KeyChar) : message;
            }

            //ReceiveService.CloseConnection();
        }

        static int Login()
        {
            int iChoise = 0;

            while (iChoise < 1 || iChoise > 3)
            {
                Write("Choose action:\n 1. Login\n 2. Register\n 3. Exit\n[1/2/3]: ");
                Int32.TryParse(ReadLine(), out iChoise);
            }

            return iChoise;
        }

        static async Task Reciver()
        {
            await new HostBuilder()
            .ConfigureServices((hostContext, services) =>
            {
                services.AddHostedService<ReceiveService>();
                services.Configure<ConsoleLifetimeOptions>(o => o.SuppressStatusMessages = true);
            })
            .RunConsoleAsync();
        }
    }
}
